module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		copy: {
			main: {
				src: ['.htaccess'],
				dest: 'build/',
			},
		},
		uglify: {
			build: {
				src: 'js/app.js',
				dest: 'build/js/app.js'
			}
		},
		htmlmin: {
			dist: {
				options: {
					removeComments: true,
					collapseWhitespace: true
				},
				files: {
					'build/index.html': 'index.html'
				}
			}
		},
		imagemin: {
			dynamic: {
				files: [{
					expand: true,
					cwd: 'img/',
					src: ['**/*.{png,jpg,gif}'],
					dest: 'build/img/'
				}]
			}
		},
		sass: {
			dist: {
				options: {
					style: 'compressed'
				},
				files: {
					'build/stylesheets/screen.css': 'sass/screen.scss'
				}
			}
		},
		csso: {
			dynamic_mappings: {
				expand: true,
				cwd: 'build/stylesheets/',
				src: ['*.css', '!*.min.css'],
				dest: 'build/stylesheets/',
				ext: '.css'
			}
		},
		jsonmin: {
			dev: {
				options: {
					stripWhitespace: true,
					stripComments: true
				},
				files: {
					"build/data/answer.json" : "data/answer.json",
					"build/data/knowledge.json" : "data/knowledge.json",
					"build/data/phones.json" : "data/phones.json"
				}
			}
		},
		watch: {
			scripts: {
				files: ['js/*.js'],
				tasks: ['uglify'],
				options: {
					spawn: false,
				},
			},
			css: {
				files: ['sass/*.scss'],
				tasks: ['sass'],
				options: {
					spawn: false,
				}
			},
		},
	});

	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-htmlmin');
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-csso');
	grunt.loadNpmTasks('grunt-jsonmin');
	grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.registerTask('default', ['copy','uglify', 'htmlmin', 'imagemin', 'sass', 'csso', 'jsonmin', 'watch']);
};