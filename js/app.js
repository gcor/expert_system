$(function () {
	"use strict";
	//Плагин табов
	$.fn.tabs = function(control){
		var element = $(this);
		control = $(control);
		element.delegate("li", "click", function(){
			var tabName = $(this).attr("data-tab");
			element.trigger("tabs", tabName);
		});
		element.on("tabs", function(e, tabName){
			element.find("li").removeClass("active");
			element.find(">[data-tab='" + tabName + "']").addClass("active");
		});
		element.on("tabs", function(e, tabName){
			control.find(">[data-tab]").removeClass("active");
			control.find(">[data-tab='" + tabName + "']").addClass("active");
		});
		var firstName = element.find(".active").attr("data-tab");
		element.trigger("tabs", firstName);
		return this;
	};
	var expert = {
		inputData:{
			minMaxAverageFeatures: {}, // средние значения характеристик
			features: {}, // выбранные характеристики
			specify: [], // для уточнения
			sortArray:["cost"], // по чему сортируем (массив цифр или строка)
			answers: {
					chat:"",
					whatphone:{ // есть ли телефон по рецепту
						exist:false,
						answer:""
					},
				answer:"", // простой ответ
				add:"", // слово для уточнения
				count:0, //количество сообщений обсуждения одной темы
				understand:{ //понятно ли сообщение
					feature:false, // характеристики
					answer:false, // ответы
					total:false // в целом
				} 
			},
		},
		output:{
			phones: [], //подходящие телефоны
			tags: {} //теги
		},
		helpers: {
			minMaxAverageFeaturesData: {},
			defineMinMaxAverageFeatures: function () {
				function Feature(features) {
				this.define = function() {  
					var i, sum, average = 0;
					for(var p in features){
						var count = 0,
							sum = 0,
							min=1000000,
							max=0;
						for (i = 0; i < features[p].length; i++) {
							if (typeof features[p][i] === "number") {
								sum = sum + features[p][i];
								count = count + 1;
								if (features[p][i] > max) max = features[p][i];
								if (features[p][i] < min) min = features[p][i];
							};
						}
						average = sum/count;
						//console.log(p, Math.round(sum/count));
						if (!isNaN(average)){
							expert.inputData.minMaxAverageFeatures[p] = {
								average: Math.round(average),
								min: min,
								max: max
							};
						}
					}
				};
				}
				var object = expert.helpers.minMaxAverageFeaturesData;
				$.getJSON("data/phones.json", function (phones) {
					phones.forEach(function (phone) {
					for (var feature in phone){
						if (typeof object[feature]==="undefined"){
							object[feature]=[];
							object[feature].push(phone[feature]);
						} else if (typeof phone[feature]==="number"){
							object[feature].push(phone[feature]);
						}
					};
					});
					//console.log(expert.inputData.minMaxAverageFeatures);
					user.define();
				});
				var user = new Feature(object);
			},
			getRandomArbitary:function (min, max){
				return Math.random() * (max - min) + min;
			},
			addTimeZero: function (i) {
				if (i < 10) i = "0" + i;
				return i;
			},
			defineHundreds:function(s) {
				var define = {
					houndred: {
						num:"00",
						words:["сотни"]
					},
					thousand:{
						num:"000",
						words:["тыс.","тысяч","косар","сотен"]
					}
				}, 
				num;
				for(var p in define){
					define[p].words.forEach(function(e){
						if(s.indexOf(" "+e)>-1){
							num = define[p].num;
							s=s.replace(" "+e,num)+" "+e;
						}
					});
				}
				return s;
			},
			defineWithout: function (s) {
				var array = ["не ", "нет", "не хочу", "отсутствует", "без "]
				for (var i = array.length - 1; i >= 0; i--) {
					if(s.indexOf(array[i])>-1) {
						s = s.replace(' и ', ',');
					}
				};
				return s;
			}
		},
		actions: {
			parseString: function (string) {
				$(".logs").empty(); //очищаем логи
				var string, //строка
					understand = expert.inputData.answers.understand,
					stringArr = [], //массив слов из строчки
					inputAnswers=expert.inputData.answers;

				// если требуется что то уточнить добавляем к строке слово для уточнения
				if (inputAnswers.add.length>0) {
					string = string+" "+inputAnswers.add;
					inputAnswers.add=[];
				}
				string = string.toLowerCase();
				string= expert.helpers.defineHundreds(string);
				string= expert.helpers.defineWithout(string);
				string = string.replace(/[.](?=\s?[А-Я|A-Z|а-я|a-z])/g, ',');// меняем . на ,
				stringArr = string.split(',');


				console.group("Строка ["+stringArr+"]");

				$.getJSON("data/knowledge.json", function (parse) {
					var wordExist=false, // существует ли слово
						stringExist=false, // существует для string
						conditionExist=false, //существует ли условие number
						booleanWord, // существует ли bool
						conditionWord, // имя условия
						existingWord, // найдинная функция
						existingWordFromVal, //от для num
						existingWordToVal, //до для num
						featureValue, // слово из string
						understand = expert.inputData.answers.understand;

					parse.forEach(function (objects) {
					// Смотрим каждую часть строки
					stringArr.forEach(function (word) {
					//ищем функцию из списка

					//если тип слова 
					if (objects.itemtype==="text"){
						for(var search in objects.words){
							objects.words[search].forEach(function (exist){
								if (word.indexOf(exist)>-1) {
									stringExist = true;
									expert.inputData.features[objects.name] = search;
									expert.output.tags[objects.name]=objects.fullname+" " + search;
								}
							});
						}
					}

					objects.define.forEach(function (exist) {
					// если нашли
					if (word.indexOf(exist)>-1) {
						wordExist = true;
						booleanWord = true;
						existingWord = objects.name;
						//смотрим тип найденной функции
						switch (objects.itemtype) {
							case "bool":
								//проверяем наличие слов об отсутствии функции
								objects.exist_false.forEach(function (existFalse) {
									//если в части строки есть слово из списка
									if (word.indexOf(existFalse)>-1) {
										booleanWord = false;
									}
								});
								//добавляем функцию existingWord телефона в объект для поиска
								if (booleanWord) {
									//для поиска
									expert.inputData.features[existingWord] = booleanWord
									//понятными словами
									expert.output.tags[existingWord]=objects.fullname;
								}
								//если найдена функция и слова, то пишем, что не нужна функция
								else if (booleanWord===false) {
									//для поиска
									expert.inputData.features[existingWord] = booleanWord
									//понятными словами
									expert.output.tags[existingWord]=objects.fullname+" отсутствует";
								}
							break;
							case "num":
								conditionExist = false;
								//если найдено слово-условие, смотрим его тип
								for (var condition in objects.words) {
									if (word.indexOf(exist)>-1) {
										objects.words[condition].forEach(function (exist) {
											if (word.indexOf(exist)>-1) {
												conditionExist=true;
												conditionWord=condition;
											}
										});
									}
								}
								//если есть слово-условие выполяем, то что нужно
								if (conditionExist) {
									wordExist = true;
									switch (conditionWord) {
										case "condition_max": 
											existingWordFromVal = expert.inputData.minMaxAverageFeatures[existingWord].average;
											existingWordToVal = expert.inputData.minMaxAverageFeatures[existingWord].max;
											expert.output.tags[existingWord]="Наибольшее: "+objects.fullname;
										break;
										case "condition_min": 
											existingWordFromVal = expert.inputData.minMaxAverageFeatures[existingWord].min;
											existingWordToVal = expert.inputData.minMaxAverageFeatures[existingWord].average;
											expert.output.tags[existingWord]="Среднее: "+objects.fullname;
										break;
										case "condition_more": 
											existingWordFromVal = Number(word.match(/\d+/));
											existingWordToVal = expert.inputData.minMaxAverageFeatures[existingWord].max;
											expert.output.tags[existingWord]=objects.fullname+" больше " + existingWordFromVal;
										break;
										case "condition_less": 
											if(!!Number(word.match(/\d+/))){
												existingWordFromVal = expert.inputData.minMaxAverageFeatures[existingWord].min;
												existingWordToVal = Number(word.match(/\d+/));
												expert.output.tags[existingWord]=objects.fullname+" меньше " + existingWordToVal;
											}
										break;
										case "condition_not": 
											existingWordFromVal=expert.inputData.minMaxAverageFeatures[existingWord].min;
											existingWordToVal=expert.inputData.minMaxAverageFeatures[existingWord].max;
											expert.output.tags[existingWord]="Любое: "+objects.fullname;
										break;
										default: console.log("нет условия"); break;
									}
								} 
								else {
									//если есть только число
									if(!!Number(word.match(/\d+/))){
										existingWordFromVal = Number(word.match(/\d+/));
										existingWordToVal = Number(word.match(/\d+/));
										expert.output.tags[existingWord]=objects.fullname+" "+existingWordFromVal;
									} 
									//если известна только функция
									else {
										expert.output.tags[existingWord]=objects.fullname;
										//отправляем на уточнение, если уже не добавлено
										if ($.inArray(existingWord, expert.inputData.specify)===-1) {
											expert.inputData.specify.push(existingWord);
											expert.inputData.answers.add=word;
										};
										existingWordFromVal = expert.inputData.minMaxAverageFeatures[existingWord].min;
										existingWordToVal=expert.inputData.minMaxAverageFeatures[existingWord].max;
									}
								}
								//если уточнили, удалям
								if ($.inArray(existingWord, expert.inputData.specify)>-1 &&
									 existingWord in expert.inputData.features)
									{
									expert.inputData.specify = expert.inputData.specify.slice(1,1);
								}
								//добавляем функцию existingWord телефона в объект для поиска
								if (existingWord && !!existingWordFromVal && !!existingWordToVal){
									expert.inputData.features[existingWord] = [existingWordFromVal,existingWordToVal];
								}
							break;
							case "phrase":
								stringExist = false;
								// смотрим на массив вариантов
								for(var search in objects.words){
									//для каждого
									objects.words[search].forEach(function (text) {
										if (word.indexOf(text)>-1) {
											stringExist = true
											featureValue = search;
										}
									});
								};
								if (wordExist && stringExist) {
									console.log(existingWord,featureValue,objects.name);
									expert.inputData.features[existingWord] = featureValue;
									expert.output.tags[existingWord]=objects.fullname+" " + featureValue;
								} 
								else if (wordExist) {
									//уточнение
									if ($.inArray(existingWord, expert.inputData.specify)===-1) {
										console.log(existingWord,featureValue,objects.name);
										expert.inputData.specify.push(existingWord);
										expert.output.tags[existingWord]=objects.fullname;
										expert.inputData.answers.add=word;
									};
								}
							break;
							case "text": console.log("Поиск выполнен"); 
							break;
							default: console.log("нету такого:" + word);
							break;
						};
					} 
					});
					});
					});
					//если ничего не найдено/найдено
					(!wordExist && !stringExist && !conditionExist) ? understand.feature = false : understand.feature = true;
					//сортируем подходящие телефоны
					expert.actions.suitablePhones();
				})
				.success(function() {$(".logs").append("<li class='log done'>База data/knowledge.json загружена</li>");})
				.error(function() { console.log("Ошибка в JSON data/knowledge.json"); $(".logs").append("<li class='log db_error'>Ошибка в базе data/knowledge.json</li>") })

				$.getJSON("data/answer.json", function (parse) {
					var answer=false,
						understand = expert.inputData.answers.understand;
					// Смотрим каждую часть строки
					stringArr.forEach(function (word) {
					parse.forEach(function (objects){
						//поиск функции
						for (var func in objects.functions){
							objects.functions[func].forEach(function (exist){
								if (word.indexOf(exist)>-1){ 
									switch(func){
										case "deleteFeatures":
											expert.inputData.features = {}
											expert.output.tags = {}
											break;
										case "deleteChat":
											expert.inputData.features = {}
											expert.output.tags = {}
											$(".me, .system").detach();
											break;
										default:
											break;
									}
								}
							});
						}
						// смотрим если есть слова уточнения
						if (expert.inputData.specify.length>0){
							var randomNum = expert.helpers.getRandomArbitary(0,objects.system.correct.specify.length);
							answer = objects.system.correct.specify[Math.floor(randomNum)] + " " + expert.output.tags[expert.inputData.specify[0]];
						} 
						// если есть специальные слова
						else {
							// если есть спецфразы
							for(var typeword in objects.system.whatPhone){
								var whatPhone = objects.system.whatPhone;
								whatPhone[typeword].words.forEach(function (exist) {
									if (word.indexOf(exist)>-1){
										var inputAnswers = expert.inputData.answers;
										var randowAnswer = expert.helpers.getRandomArbitary(0,whatPhone[typeword].answer.length);
										if (answer===false){answer=""}
										answer = answer + whatPhone[typeword].answer[Math.floor(randowAnswer)];
										inputAnswers.add = whatPhone[typeword].add;
										inputAnswers.whatphone.exist=true;
										inputAnswers.whatphone.answer=answer;
										expert.actions.addMessage();
									}
								})
							}
							// если есть слова для общения
							for(var typeword in objects.chat){
								objects.chat[typeword].words.forEach(function (exist) {
									if (word.indexOf(exist)>-1){
										//количество ответов
										expert.inputData.answers.count = document.getElementsByClassName('system').length+1;
										var randowAnswer = expert.helpers.getRandomArbitary(0,objects.chat[typeword].answer.length)
										randowAnswer = Math.floor(randowAnswer);
										//если количество слов 1 и привет есть и функций еще нет, то расширенный привет
										if (stringArr.length===1 &&
											 $.inArray(word, objects.chat.greeting.words)>-1 &&
											 Object.keys(expert.inputData.features).length===0) 
										{
											var randomNum = expert.helpers.getRandomArbitary(0,objects.chat.greeting.one_in_string.length);
											answer = objects.chat.greeting.one_in_string[Math.floor(randomNum)];
										} 
										else if (stringArr.length===1 && $.inArray(word, objects.chat.wantphone.words)>-1 && Object.keys(expert.inputData.features).length===0) {
											var randomNum = expert.helpers.getRandomArbitary(0,objects.chat.wantphone.one_in_string.length);
											answer = objects.chat.wantphone.one_in_string[Math.floor(randomNum)];
										} 
										else if (answer!==undefined && answer!==false) { 
											answer = answer+objects.chat[typeword].answer[randowAnswer];
										} 
										//создаём объект, если его нет
										else { 
											answer = objects.chat[typeword].answer[randowAnswer]; 
										}
									}
								});
							}
						}
					});
					});
					//добавляем ответ в объект
					if (!!answer) expert.inputData.answers.chat=answer;
					else expert.inputData.answers.chat=""
					//если ничего не найдено/найлено
					answer===false ? understand.answer = false : understand.answer = true;
				})
				.success(function() {$(".logs").append("<li class='log done'>База data/answer.json загружена</li>");})
				.error(function() { console.log("Ошибка в JSON data/answer.json"); $(".logs").append("<li class='log db_error'>Ошибка в базе data/answer.json</li>") })

				//даём базе поработать и получаем финальный ответ, поняли ли мы строчку
				setTimeout(function() {
					(understand.feature===true || understand.answer===true) ? understand.total=true : understand.total=false
				}, 200);
			},
			suitablePhones: function () {
				//отбираем выбранные телефоны 
				$.getJSON("data/phones.json", function (phones) {
					expert.output.phones=[];
					phones.forEach(function (phone) {
					var addPhone=[];
					for (var feature in expert.inputData.features) {
						switch(typeof expert.inputData.features[feature]){
							case "boolean":
								//если значения объекта совпадают с харакристикой телефона — выводим
								if (typeof expert.inputData.features[feature]==="boolean" && expert.inputData.features[feature]===phone[feature]) {
									addPhone.push(true);
								} else addPhone.push(false);
							break;

							case "object":
								if (typeof phone[feature] === "number" && expert.inputData.features[feature][0]<=phone[feature] && phone[feature]<=expert.inputData.features[feature][1]) {
									addPhone.push(true);
								} else {
									addPhone.push(false);
								}
							break;

							case "string":
								if (typeof phone[feature]==="string" && expert.inputData.features[feature].toLowerCase()===phone[feature].toLowerCase() ) {
									addPhone.push(true);
								} else{
									addPhone.push(false);
								}
							break;
						} 
					}
					if ($.inArray(phone.model, expert.output.phones)===-1 && $.inArray(false, addPhone)===-1) {
						expert.output.phones.push(phone);
					}
					});
					//сортируем по параметрам
					expert.output.phones.sort(function (a, b){
						var one = 1, two = 1; 
						for (var i = expert.inputData.sortArray.length - 1; i >= 0; i--) {
							one = one*a[expert.inputData.sortArray[i]];
							two = two*b[expert.inputData.sortArray[i]];
						};
						if (one > two) return -1; 
						else if (one < two) return 1; 
						else return 0; 
					});
					expert.actions.showPhone(phones);
				})
				.success(function() {$(".logs").append("<li class='log done'>База data/phones.json загружена</li>");})
				.error(function() { console.log("Ошибка в JSON data/phones.json"); $(".logs").append("<li class='log db_error'>Ошибка в базе data/phones.json</li>") })
			},
			showPhone: function (phones) {
				$(".logs").append("<li class='log ok'>Сообщений "+(document.getElementsByClassName('system').length+1)+", выбрано телефонов "+expert.output.phones.length+"</li>");
				$(".logs").append("<li class='log ok'>Характеристик "+Object.keys(expert.inputData.features).length+". "+Object.keys(expert.inputData.features).toString()+"</li>");
				//	console.log("Сообщений",document.getElementsByClassName('system').length+1, "Телефонов",expert.output.phones.length,"Теги",expert.output.tags);
				//	console.log("Характеристики",expert.inputData.features);
				//	if (expert.inputData.specify.length>0) console.log("Стоит уточнить: " + expert.inputData.specify);
				//	if(!!expert.inputData.answers) console.log("Эксперт", expert.inputData.answers);
				// console.log(expert.inputData.answers.understand.total?"ясно":"не ясно");

				$(".phones").detach();
				var phonesList = $("<ul class='phones'>");
				expert.output.phones.forEach(function (phone) {
					var phoneItem = $("<li class='phone'>");
					var phoneImage = '<img src="'+phone.imagefull+'" alt="'+phone.company+' '+phone.model+'" />';

					if (!!phone.cores && !!phone.processor && !!phone.ram) { 
						var phoneProcessor = 'Процессор '+phone.cores+'×'+phone.processor+'МГц, ОЗУ '+phone.ram
					} else var phoneProcessor='';

					if (!!phone.camera && !!phone.wifi) { 
						if (!!phone.lte===false){
							var phoneCamera = ', камера '+phone.camera+'мп'+', wifi'
						}
						else{
							var phoneCamera = ', камера '+phone.camera+'мп'+', wifi, LTE'
						}
					} else var phoneCamera='';

					var returnDesc = '<div class="phone-fullimg"><div class="phone-fullimg__block"><div class="phone-fullimg__close"><span>×</span></div>'+
										'<img src="'+phone.imagefull+'" alt="'+phone.company+' '+phone.model+'"></div></div>'+
										'<div class="phone__img">'+phoneImage+'</div>'+
										'<div class="phone__desc"><div class="feature title">'+phone.company+' '+phone.model+'</div>'+
										'<div class="feature cost" >'+ phone.cost+' руб.</div>'+
										'<div class="feature">'+'Экран '+phone.diagonal+phoneCamera+'</div>'+
										'<div class="feature">'+ phoneProcessor+'</div>'+
										'<div class="feature">'+'Батарея '+phone.battery+'мАч'+'</div>';
					phoneItem.html(returnDesc);
					phonesList.append(phoneItem);
				});

				$(".tags").detach();
				var tagsList = $("<ul class='tags'>");
				if (Object.keys(expert.output.tags).length>0) {window.scrollTo(0, 0);}
				for (var tag in expert.output.tags) {
					 var tagsItem = $("<li class='tag'>"),
						 returnTag = expert.output.tags[tag];
					tagsItem.html("<span class='tag__text'>"+returnTag+"</span><span class='tag__delete'> x</span>");
					tagsList.append(tagsItem);
				};

				$(".recommend .tags-list").append(tagsList);
				$(".recommend .phones-list").append(phonesList);
				$(".tag__delete").on("click",function() {
					// Удаление тега вместе со свойством объекта
					var deleteTagName = $(this).parent().find(".tag__text").text();
					for(var deleteTag in expert.output.tags){
						if (expert.output.tags[deleteTag]===deleteTagName){
							var deleteWhereTag = deleteTag;
						}
					}
					delete expert.output.tags[deleteWhereTag];
					delete expert.inputData.features[deleteWhereTag];
					//console.log("Удалено: "+deleteTagName,deleteWhereTag,expert.inputData.features, expert.output.tags);
					$(this).parent().detach();
					expert.actions.suitablePhones();
				});
				$('.phone__img').on('click', function() {
					var fullimg = $(this).parent().find(".phone-fullimg");
					fullimg.show();
				});
				$('.phone-fullimg').on('click', function() {
					var fullimg = $(this).parent().find(".phone-fullimg");
					fullimg.hide();
				});
			},
			getAnswer: function (obj,arr) { 
				expert.inputData.answers.answer = "";
				//получить рэндомный ответ по заданному объекту-массиву
				$.getJSON("data/answer.json", function (parse) {
					parse.forEach(function (objects){
						var randowAnswer = expert.helpers.getRandomArbitary(0,objects.system[obj][arr].length);
						expert.inputData.answers.answer = objects.system[obj][arr][Math.floor(randowAnswer)];
					});
				})
				.success(function() {$(".logs").append("<li class='log done'>База data/answer.json загружена для подбора ответа</li>");})
				.error(function() { console.log("Ошибка в JSON data/answer.json"); $(".logs").append("<li class='log db_error'>Ошибка в базе data/answer.json</li>") })
				return expert.inputData.answers.answer; 
			},
			say: function () {
				var features = expert.inputData.features, // выбранные функции
					 phones = expert.output.phones, // выбранные телефоны
					 specialAnswer = expert.inputData.answers,
					 understand=expert.inputData.answers.understand.total,
					 answer="",//
					 date = new Date;

				date = expert.helpers.addTimeZero(date.getHours())+":"+expert.helpers.addTimeZero(date.getMinutes());
				//Если вообще ничего не понятно
				if(!understand && !expert.inputData.answers.whatphone.exist) {
					console.log("ничего непонятно");
					answer = expert.actions.getAnswer("correct","notclear");
					if (answer==="") answer="На улице бывает ясно, а мне сейчас нет.." //велосипед для таймера, а то первый раз не работает из за несинхронизации
				} 
				//Если что то понятно
				else {
					//Если есть уточнение
					if (expert.inputData.specify.length>0 && expert.inputData.answers.count<2 ) {
						console.log("есть какое то уточнение");
						answer=specialAnswer.chat;
						specialAnswer.chat = ''; //обнуляем
					} 
					//ничего не уточняем
					else {
						//если есть ответы на специальные вопросы (привет пока)
						if (specialAnswer.chat!==undefined && specialAnswer.chat!=="" && specialAnswer!=="" || !!expert.inputData.answers.whatphone.exist) {
							console.log("есть фразы специальные");
							answer=specialAnswer.chat + " " + expert.inputData.answers.whatphone.answer;
							expert.inputData.answers.whatphone.exist=false;
						}
						else {
							console.log("выборка");
							//Подобрана выборка телефонов
							switch(Object.keys(phones).length){
								case 0:
									answer = answer + ' У нас нет в базе такого телефона, спросите что полегче)';
									break;
								case 1:
									var phone=phones[0];
									answer = answer + ' Советую ' + phone.company+" "+phone.model+". По цене "+phone.cost+" руб. Батарейка целых "+phone.battery+" мАч";
									break;
								case 2:
									var phone1=phones[0];
									var phone2=phones[1];
									answer = answer + ' Посоветую два интересных телефона: ' + phone1.company+" "+phone1.model+" и "+ phone2.company+" "+phone2.model;
									break;
								case 3:
									var phone1=phones[0];
									var phone2=phones[1];
									var phone3=phones[2];
									answer = answer + ' Подскажу 3 телефона — '+ phone1.company+" "+phone1.model+", "+ phone2.company+" "+phone2.model+" и "+ phone3.company+" "+phone3.model;
									break;
								default:
									if(Object.keys(expert.inputData.features).length===0){
										answer = 'По вашему запросу целых '+phones.length+' тел. '+expert.actions.getAnswer("correct","correctQuestion");;
										if (expert.inputData.answers.answer==="") {
											//велосипед для таймера, а то первый раз не работает из за несинхронизации
											answer = 'По вашему запросу целых '+phones.length+' тел. Что нибудь еще может быть?';
										}
										break;
									}
									else{
										var tags="";
										for(var tag in expert.output.tags){
											tags=tags+", "+expert.output.tags[tag];
										}
										tags=tags.substr(1);
										answer = phones.length+' тел. '+expert.actions.getAnswer("correct","correctAnswerWhisFeature");+" "+tags;
										if (expert.inputData.answers.answer==="") {
											//велосипед для таймера, а то первый раз не работает из за несинхронизации
											answer = 'По вашему запросу целых '+phones.length+' тел. Что нибудь еще может быть?' 
										}
										break;
									}
							}
						}
					}
				}
				$('.messages').prepend('<li class="message"><div class="system"><div class="date">'+date+'</div><div class="text">'+answer+'</div></div></li>');
			},
			addMessage: function () {
				var send = $('.send'),
					input = $('textarea.input'),
					message = input.val().trim(),
					date = new Date;
						
					date = expert.helpers.addTimeZero(date.getHours())+":"+expert.helpers.addTimeZero(date.getMinutes());
					expert.actions.parseString(message);

				if (message !== "") {
					input.val('');
					$('.message_none').detach();
					$('.messages').prepend('<li class="message"><div class="me"><div class="date">'+date+'</div><div class="text">'+message+'</div></div></li>');
					$('.message_loader').show();
					input.prop('readonly', true).css("opacity","0.4").attr("placeholder","Загрузка...");
					send.css("opacity","0.4");
					var features = expert.inputData.features;
					var phones = expert.output.phones;

					setTimeout(function() {
						expert.actions.say();
						$('.message_loader').hide();
						input.prop('readonly', false).css("opacity","1").attr("placeholder","");;
						send.css("opacity","1");
					}, 500);
				} 
				else {
					//input.css("border","1px solid red");
				};
				console.groupEnd();
			}
		},
		init: function () {

			//форма добавления рецепта выбаёт строку json
			var form=document.getElementById("add");
			form.onsubmit=function () {
				var recipeTitle = document.getElementById("recipe-title").value,
					recipeWords = document.getElementById("recipe-words").value,
					recipeAdd = document.getElementById("recipe-add").value,
					recipeAnswer = document.getElementById("recipe-answer").value;

				recipeWords = recipeWords.replace(/[,](?=\s?[А-Я|A-Z|а-я|a-z])/g, '","');
				recipeAdd = recipeAdd.replace(/[,](?=\s?[А-Я|A-Z|а-я|a-z])/g, '","');

				var json='"'+recipeTitle+'":{'+
					'"words":'+'["'+recipeWords+'"],'+
					'"add":'+'["'+recipeAdd+'"],'+
					'"answer":'+'["'+recipeAnswer+'"]'+
				'}';
				alert(json);
			}

			expert.helpers.defineMinMaxAverageFeatures();
			expert.actions.suitablePhones();
			$(".tabs").tabs(".sections");
			$('.input').on('keypress', function(e){
				if(e.keyCode === 13) {
					expert.actions.addMessage();
				}
			});
			$('.send').on('click', function(){
				expert.actions.addMessage();
			});
		}
	}
	expert.init();
});